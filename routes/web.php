<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin','middleware' => 'check_file_extentions'], function () {
    Voyager::routes();
});

Route::get('users/export_excel', 'Voyager\VoyagerBaseController@export_excel');
Route::get('users/export_excel_users', 'Voyager\VoyagerBaseController@export_excel_users');
Route::get('users/export_pdf', 'Voyager\VoyagerBaseController@export_pdf');

