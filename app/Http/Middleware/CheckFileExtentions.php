<?php

namespace App\Http\Middleware;

use App\Helpers\Output;
use App\public_classes\SystemResponse;
use Closure;

class CheckFileExtentions
{
    private $availableExt = ['jpeg','jpg','png','gif','pdf','csv','doc','docx','txt','ppt','pptx','mp3','mp4','xls','xlsx','svg','mov'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $files = $request->files;

        $isRightExt = $this->checkExt($files);

        if(!$isRightExt)
            return response()->json('wrong extension file',422);

        return $next($request);
    }

    private  function checkExt($files)
    {
        foreach ($files as $key => $value) {

            if(!is_array($value) && $value->isFile()) {
                $ext = $value->getClientOriginalExtension();
                if (!in_array(strtolower($ext), $this->availableExt))
                    return false;
            }
            else {

                if(is_array($value) || is_object($value)) {
                    $isRightExt = $this->checkExt($value);
                    if (!$isRightExt)
                        return false;
                }
            }

        }

        return true;
    }
}
