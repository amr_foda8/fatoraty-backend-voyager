<?php

namespace App\Http\Controllers\Voyager;

use App\Invoice;
use App\MonthlySubscription;
use App\User;
use App\Warranty;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;
use Illuminate\Http\Request;
use PDF;

class VoyagerBaseController extends BaseVoyagerBaseController
{
    public function export_excel(Request $request)
    {
        $exportObject = new $request->slug($request);

        $data = $exportObject->collection();
        return (new FastExcel($data))->download('file.xlsx');

//        $object = new $request->slug($request);
//
//        return Excel::download($object,'data.xlsx');
//        Excel::store($object,'data.xlsx','voyager');
//        return back();
    }


    public function export_excel_users(Request $request)
    {

        $users = ((new FastExcel(User::withCount(['invoices', 'warranties', 'monthly_subscriptions'])->get()))->export('users.csv', function ($user) {
            return [
                'الاسم' => $user->name,
                'البريد الالكتروني' => $user->email,
                'رقم الهاتف' => $user->phone_number,
                'الحالة' => $user->status,
                'توقيت الاضافة' => strval($user->created_at),
                'عدد الفواتير' => $user->invoices_count,
                'عدد الضمانات' => $user->warranties_count,
                'عدد الاشتراكات' => $user->monthly_subscriptions_count,

            ];
        }));

        return response()->download($users);

    }

    public function export_pdf(Request $request)
    {
        $exportObject = new $request->slug($request);

        $data = $exportObject->collection();
        $data = $data->slice(0,2000);
        $columns = $exportObject->getTableColumns();

        $pdf = PDF::loadView('pdfTemplate',  compact('data','columns'));
        return $pdf->download('data.pdf');

    }
}
