<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MonthlySubscriptionController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WarrantyController;
use App\User;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;
use TCG\Voyager\Facades\Voyager;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {
        $topInvoiceUsers = User::withCount('invoices','warranties')->withSum('monthly_subscriptions','total_value')
            ->orderBy('invoices_count','desc')->limit(100)->get();
        
        $topWarrantyUsers = User::withCount('warranties','invoices')->withSum('monthly_subscriptions','total_value')
            ->orderBy('warranties_count','desc')->limit(100)->get();


        $data = new \stdClass();
        $data->invoice = InvoiceController::getChartData();
        $data->monthlySubscription = MonthlySubscriptionController::getChartData();
        $data->review = ReviewController::getChartData();
        $data->users = UserController::getChartData();
        $data->warranties = WarrantyController::getChartData();
        return Voyager::view('voyager::index',compact('topInvoiceUsers','topWarrantyUsers'
            ,'data'));
    }
}
