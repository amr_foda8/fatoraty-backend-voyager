<?php

namespace App\Http\Controllers;

use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public static function getChartData()
    {
        $output = new \stdclass();

        $Chart = Review::orderBy('created_at','DESC')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('Y-M');
            });

        $Keys = [];
        $Avg = [];

        $cnt = 0;
        foreach ($Chart as $key => $value) {
            if($cnt == 12)
                break;
            $Keys[] = $key;

            $totalValueSum = 0;
            $totalValueCount = 0;
            foreach ($value as $val) {
                $totalValueSum = $totalValueSum + $val->total_reviews;
                $totalValueCount++;
            }
            $Avg[] = $totalValueSum/$totalValueCount;
            $cnt++;
        }

        $output->dataKeys = array_reverse($Keys);
        $output->dataAverages = array_reverse($Avg);
        $output->dataName = 'احصائية التقيمات';

        return $output;
    }
}
