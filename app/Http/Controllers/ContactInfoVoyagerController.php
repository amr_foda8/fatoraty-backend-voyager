<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

class ContactInfoVoyagerController extends  \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function gbsLinkFetch($address)
    {
        $match=$this->MatchChecker($address);
        if(Count($match)<2)
        {
            try {
                $match = "";
                $address = $this->ger_origenal_url($address);
                $match = $this->MatchChecker($address);
            } catch (\Exception $e) {
                $OUT['status']['statusCode']="500";
                $OUT['status']['LastURL']=$address;
                $OUT['status']['message']="Unkonwn Error" ;
            }
        }
        if(Count($match)>2)
        {
            $OUT['status']['statusCode']="200";
            $OUT['status']['message']="Fetch Successfully" ;
            $OUT['Data']['Latitude']=$match[1];
            $OUT['Data']['Longitude']=$match[2];
        }
        else
        {
            $OUT['status']['statusCode']="500";
            $OUT['status']['LastURL']=$address;
            $OUT['status']['message']="Unkonwn Error" ;
        }
        return $OUT;
    }
    private function MatchChecker($address)
    {
        preg_match('/@(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/', $address, $match );
        if(Count($match)<2)
            preg_match('/q=(\-?[0-9]+\.[0-9]+),(\-?[0-9]+\.[0-9]+)/', $address, $match );
        return $match;
    }
    private function ger_origenal_url($url)
    {
        $shortURL = $url;
        if  ( $ret = parse_url($shortURL) ) {
            if ( !isset($ret["scheme"]) ){
                $shortURL = "http://{$shortURL}";
            }
        }
        $result = array(
            'success' => false,
            'message' => 'Bad URL',
            'data'=> $shortURL
        );
        $header = @get_headers($shortURL, 1);
        $is_shortURL = true;
        if(isset($header['Location'])){
            $real_url = $header['Location'];
        }else if(isset($header['location'])){
            $real_url = $header['location'];
        }else{
            $real_url = '';
            $is_shortURL = false;
        }
        if(is_array($real_url)){
            $real_url = $real_url[0];
        }
        if($is_shortURL){
            $domain = parse_url($real_url);
            $domain = $domain['host'];
            return $real_url;
        }
        return $url;
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        $location = $this->gbsLinkFetch($request->map_link_address);

        if($location['status']['statusCode'] != "200")
        {
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.error_updating')." {$data->map_link}",
                    'alert-type' => 'error',
                ]);
        }
        else
        {
            $request['map_link'] = $location['Data']['Latitude'] . ", " . $location['Data']['Longitude'];
        }
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
    }
}
