<?php

namespace App\Http\Controllers;

use App\MonthlySubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MonthlySubscriptionController extends Controller
{
    public static function getChartData()
    {
        $output = new \stdclass();

        $Chart = MonthlySubscription::orderBy('created_at','DESC')
            ->where('draft',0)
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('Y-M');
            });

        $Keys = [];
        $Counts = [];

        $cnt = 0;
        foreach ($Chart as $key => $value) {
            if($cnt == 12)
                break;
            $Keys[] = $key;
            $Counts[] = count($value);
            $cnt++;
        }

        $output->dataKeys = array_reverse($Keys);
        $output->dataCounts = array_reverse($Counts);
        $output->dataName = 'عدد الاشتراكات الشهرية';

        return $output;
    }
}
