<?php

namespace App\Http\Controllers;

use App\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public static function getChartData()
    {
        $output = new \stdclass();

        $invoicesChart = Invoice::orderBy('date','DESC')
            ->where('draft',0)
            ->whereNotNull('date')
            ->where('date','<',Carbon::now()->endOfMonth()->toDateString())
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->date)->format('Y-M');
            });
        $invoiceKeys = [];
        $invoiceCounts = [];

        $cnt = 0;
        foreach ($invoicesChart as $key => $value) {
            if($cnt == 12)
                break;
            $invoiceKeys[] = $key;
            $invoiceCounts[] = count($value);
            $cnt++;
        }

        $output->dataKeys = array_reverse($invoiceKeys);
        $output->dataCounts = array_reverse($invoiceCounts);
        $output->dataName = 'عدد الفواتير';

        return $output;
    }
}
