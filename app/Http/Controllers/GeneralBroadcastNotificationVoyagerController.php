<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Pusher\PushNotifications\PushNotifications;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;

class GeneralBroadcastNotificationVoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request)
    {
//        $this->notify(["165725"], $request->title_en, $request->body_en);
//        $this->notify(["166354"], $request->title, $request->body);
//        dd('notified');
//
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));


        //$userIds = User::whereIn('id', [165725, 166354, 166528])->with('settingss')->get();
        $userIds = User::with('settingss')->get();
        $userIdsArr = [];
        foreach ($userIds as $userId) {
            if(sizeof($userId->settingss) > 0) {
                if ($userId->settingss[0]->language == 'en')
                    $userIdsArr[] = strval($userId->id);
            }

            if(sizeof($userIdsArr) == 100)
            {
                $this->notify($userIdsArr, $request->title_en, $request->body_en);
                $userIdsArr = [];
            }
        }
        if(sizeof($userIdsArr) > 0)
            $this->notify($userIdsArr, $request->title_en, $request->body_en);

        $userIdsArr = []; //empty arr to avoid sending duplicate notifications
        foreach ($userIds as $userId) {
            if(sizeof($userId->settingss) > 0) {
                if ($userId->settingss[0]->language == 'ar')
                    $userIdsArr[] = strval($userId->id);
            }

            if(sizeof($userIdsArr) == 100)
            {
                $this->notify($userIdsArr, $request->title, $request->body);
                $userIdsArr = [];
            }
        }
        if(sizeof($userIdsArr) > 0)
            $this->notify($userIdsArr, $request->title, $request->body);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
    }

    public function notify($userIds, $title , $body)
    {
        $beamsClient = new PushNotifications(array(
            "instanceId" => env('PUSHER_APP_INSTANCEID'),
            "secretKey" => env('PUSHER_APP_SECRETKEY'),
        ));
        $publishResponse = $beamsClient->publishToInterests(
            $userIds,
            array(
                "fcm" => array(
//                    "notification" => array(
//                        "title" => $title,
//                        "body" => $body
//                    ),
                    "data" => array(
                        "title" => $title,
                        "body" => $body
//                        "VVV" => "VVV"
                    )
                ),
                "apns" => array("aps" => array(
                    "alert" => array(
                        "title" => $title,
                        "body" => $body
                    )
                ),"data" => ['document_id'=>0, 'document_type'=>'general']
                )
            ));
    }
}
