<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WelcomeMessage extends Model
{
    protected $table = "welcome_messages";
}
