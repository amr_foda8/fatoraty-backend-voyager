<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePageVideo extends Model
{
    protected $table = "home_page_video";
}
