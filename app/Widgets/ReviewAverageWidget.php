<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/17/2019
 * Time: 12:05 PM
 */

namespace App\Widgets;
use App\Review;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class ReviewAverageWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Review::avg('total_reviews');
        $string = 'إحصائية التقييمات';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-star-half',
            'title'  => "{$count} {$string}",
            'text'   => '',
            'button' => [
                'text' => 'عرض جميع التقيمات',
                'link' => route('voyager.reviews.index'),
            ],
            'image' => '/widgets/fatoraty-logo.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
