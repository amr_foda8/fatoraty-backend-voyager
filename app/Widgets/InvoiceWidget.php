<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/17/2019
 * Time: 10:59 AM
 */

namespace App\Widgets;

use App\Invoice;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class InvoiceWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Invoice::where('draft',0)->count();
        $string = 'الفواتير';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$count} {$string}",
            'text'   => '',
            'button' => [
                'text' => 'عرض جميع الفواتير',
                'link' => route('voyager.invoices.index'),
            ],
            'image' => '/widgets/fatoraty-logo.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
