<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/17/2019
 * Time: 11:46 AM
 */

namespace App\Widgets;

use App\Warranty;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class WarrantiesWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Warranty::where('draft',0)->count();
        $string = 'الضمانات';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-certificate',
            'title'  => "{$count} {$string}",
            'text'   => '',
            'button' => [
                'text' => 'عرض جميع الضمانات',
                'link' => route('voyager.warranties.index'),
            ],
            'image' => '/widgets/fatoraty-logo.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
