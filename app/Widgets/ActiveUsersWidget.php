<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/17/2019
 * Time: 11:29 AM
 */

namespace App\Widgets;

use App\User;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class ActiveUsersWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $count = User::where('status','active')->count();
        $string = 'الحسابات المفعلة';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => '',
            'button' => [
                'text' => 'عرض جميع الاعضاء',
                'link' => '/admin/users?key=status&filter=equals&s=verified',
            ],
            'image' => '/widgets/fatoraty-logo.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
