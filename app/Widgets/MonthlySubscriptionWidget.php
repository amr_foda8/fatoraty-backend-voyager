<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/17/2019
 * Time: 11:56 AM
 */

namespace App\Widgets;

use App\MonthlySubscription;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class MonthlySubscriptionWidget extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = MonthlySubscription::where('draft',0)->count();
        $string = ' اشتراكات الشهرية';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-ticket',
            'title'  => "{$count} {$string}",
            'text'   => '',
            'button' => [
                'text' => 'عرض جميع الشتراكات الشهرية',
                'link' => route('voyager.monthly-subscriptions.index'),
            ],
            'image' => '/widgets/fatoraty-logo.png',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
