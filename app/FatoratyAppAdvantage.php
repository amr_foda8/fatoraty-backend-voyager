<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FatoratyAppAdvantage extends Model
{
    protected $table = "fatoraty_app_advantages";
}
