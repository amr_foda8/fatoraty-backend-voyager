<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlySubscription extends Model
{
    protected $table = "monthly_subscriptions";
}
