<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralBroadcastNotification extends Model
{
    protected $table = "general_broadcast_notifications";
}
