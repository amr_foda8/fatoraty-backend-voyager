<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function invoices(){
        return $this->hasMany(Invoice::class);
    }

    public function settingss(){
        return $this->hasMany(Setting::class);
    }

    public function warranties(){
        return $this->hasMany(Warranty::class);
    }

    public function monthly_subscriptions(){
        return $this->hasMany(MonthlySubscription::class);
    }

    public function conversations(){
        return $this->hasMany(Conversation::class);
    }
}
