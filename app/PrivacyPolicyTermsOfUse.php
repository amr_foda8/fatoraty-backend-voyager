<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyPolicyTermsOfUse extends Model
{
    protected $table = "privacy_policy_and_terms_of_use";
}
