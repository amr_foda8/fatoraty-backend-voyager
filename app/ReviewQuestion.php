<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewQuestion extends Model
{
    protected $table = "review_questions";
}
