<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackOfficeNotification extends Model
{
    protected $table = "back_office_notifications";
}
