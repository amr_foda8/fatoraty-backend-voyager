<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 4:10 PM
 */

namespace App\Exports;

use App\Warranty;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class warranties implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return Warranty::join('users', 'warranties.user_id', '=', 'users.id')
            ->select('users.name','users.phone_number','warranties.agent_name','warranties.warranty_start','warranties.warranty_end')
            ->get();
    }

    public function getTableColumns() {
        return array('name','phone_number','agent_name','warranty_start','warranty_end');
    }
}