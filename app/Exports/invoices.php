<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 3:35 PM
 */

namespace App\Exports;


use App\Invoice;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class invoices implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return Invoice::join('users', 'invoices.user_id', '=', 'users.id')
            ->select('users.name','users.phone_number','invoices.date','invoices.seller_name','invoices.number')
            ->get();
    }

    public function getTableColumns() {
        return array('name','phone_number','date','seller_name','number');
    }
}