<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 3:35 PM
 */

namespace App\Exports;


use App\Invoice;
use App\MonthlySubscription;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class monthly_subscriptions implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return MonthlySubscription::join('users', 'monthly_subscriptions.user_id', '=', 'users.id')
            ->select('name AS الاسم', 'phone_number AS رقم التليفون', 'service_provider AS مزود الخدمة', 'plan_name AS اسم الاشتراك', 'total_value AS اجمالي القيمة', 'from AS تاريخ البدء')
            ->get();
    }

    public function getTableColumns() {
        return array('name','phone_number','date','seller_name','number');
    }
}
