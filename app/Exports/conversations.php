<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 4:34 PM
 */

namespace App\Exports;

use App\Conversation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class conversations implements FromCollection
{
    private $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return Conversation::join('users', 'conversations.user_id', '=', 'users.id')
            ->select('users.name','users.phone_number','conversations.direction','conversations.content_ar'
                ,'conversations.content_en','conversations.created_at')
            ->where('conversations.user_id',$this->request->user_id)
            ->get();
    }

    public function getTableColumns()
    {
        return array('name','phone_number','direction','content_ar'
        ,'content_en','created_at');
    }
}