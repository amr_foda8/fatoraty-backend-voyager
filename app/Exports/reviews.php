<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 4:18 PM
 */

namespace App\Exports;

use App\Review;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;

class reviews implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return Review::join('users', 'reviews.user_id', '=', 'users.id')
            ->join('products', 'reviews.product_id', '=', 'products.id')
            ->select('users.name as user_name','users.phone_number','products.name as product_name'
                ,'reviews.review','reviews.total_reviews')
            ->get();
    }

    public function getTableColumns() {
        return array('user_name','phone_number','product_name'
        ,'review','total_reviews');
    }
}