<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 4:14 PM
 */

namespace App\Exports;

use App\Awareness;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class awarenesses implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return Awareness::select('title','description','likes')
            ->get();
    }

    public function getTableColumns() {
        return array('title','description','likes');
    }
}