<?php
/**
 * Created by PhpStorm.
 * User: foda
 * Date: 9/24/2019
 * Time: 12:40 PM
 */

namespace App\Exports;


use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;

class users implements FromCollection
{
    private $request;
    function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
//        $returnArr = [];
//
//        $returnArr[] = array('name'=>'الاسم','email'=>'البريد الالكترونى'
//        ,'created_at'=>'تاريخ الانشاء','phone_number'=>'رقم الجوال','invoices_count'=>'عدد الفواتير'
//        ,'warranties_count'=>'عدد الضمانات','monthly_subscriptions_sum'=>'مجموع الشتراكات الشهرية');
        return User::select('name','email','created_at','phone_number')
            ->withCount('invoices','warranties','monthly_subscriptions')
            ->get();
    }

    public function getTableColumns() {
        return array('name','email','created_at','phone_number','invoices_count','warranties_count','monthly_subscriptions_count');
    }
}
