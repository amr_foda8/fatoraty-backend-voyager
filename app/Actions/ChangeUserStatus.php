<?php
/**
 * Created by PhpStorm.
 * User: seamlabs
 * Date: 8/28/2019
 * Time: 7:56 PM
 */

namespace App\Actions;
use TCG\Voyager\Actions\AbstractAction;


class ChangeUserStatus extends AbstractAction
{

    public function getTitle()
    {
//        return __('voyager::generic.add_content');
        if($this->data->status == 'active')
            return 'Deactivate';
        elseif($this->data->status != 'active')
            return 'active';
    }

    public function getIcon()
    {
        return '';
    }
    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        if($this->data->status == 'active')
            return [
                'class' => 'btn btn-success btn-danger',
            ];
        elseif($this->data->status != 'active')
            return [
                'class' => 'btn btn-success btn-add-new',
            ];
    }
    public function getDefaultRoute()
    {
        if($this->data->status == 'active') {
            $action = 'deactivate';
        }
        elseif($this->data->status != 'active') {
            $action = 'active';
        }
        return route('voyager.users.index',['user_id' => $this->data->id , 'action' => $action]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }
}