<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParagraphPage extends Model
{
    protected $table = "paragraph_pages";
}
