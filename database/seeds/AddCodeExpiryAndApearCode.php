<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class AddCodeExpiryAndApearCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_id = DataType::whereName('users')->first()->id;
        $random_code = DataRow::whereField('random_code')->where('data_type_id',$table_id)->first();
        ///update to appear code random in backOffice
        $random_code->update([ "browse" => 1, "read" => 1]);
        // add code_expiry in user
        try {
            DataRow::create([
                "data_type_id" => $table_id,
                "field" => "code_expiry",
                "type" => "text",
                "display_name" => "code_expiry",
                "required" => 0,
                "browse" => 1,
                "read" => 1,
                "edit" => 0,
                "add" => 0,
                "delete" => 0,
                "order" => 28
            ]);
        }catch (Exception $exception){
            \Illuminate\Support\Facades\DB::rollBack();
        }
    }
}
