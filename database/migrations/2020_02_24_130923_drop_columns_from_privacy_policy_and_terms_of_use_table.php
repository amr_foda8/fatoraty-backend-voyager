<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsFromPrivacyPolicyAndTermsOfUseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('privacy_policy_and_terms_of_use', function (Blueprint $table) {
            $table->dropColumn('title_ar');
            $table->dropColumn('title_en');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('privacy_policy_and_terms_of_use', function (Blueprint $table) {
            //
        });
    }
}
