<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParagraphPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paragraph_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('text')->nullable();
            $table->text('text_en')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('paragraph_pages')->insert(array(
                array('name'=>'من نحن'),
                array('name'=>'سياسة االستخدام والخصوصية'),
                array('name'=>'حول التطبيق')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paragraph_pages');
    }
}
