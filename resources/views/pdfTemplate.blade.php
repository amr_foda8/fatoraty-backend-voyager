<!DOCTYPE html>
<html>
<head>
    <style>
        /*body { font-family: DejaVu Serif, sans-serif; }*/
        #pdfTable {
            font-family: DejaVu Serif, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #pdfTable td, #pdfTable th {
            border: 1px solid #ddd;
            padding: 8px;
            font-family: DejaVu Sans, sans-serif;
        }

        #pdfTable tr:nth-child(even){background-color: #f2f2f2;  text-align: right; font-family: DejaVu Sans, sans-serif;}

        #pdfTable tr:hover {background-color: #ddd; font-family: DejaVu Sans, sans-serif;}

        #pdfTable th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: right;
            background-color: #4CAF50;
            color: white;
            font-family: DejaVu Sans, sans-serif;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>

<table id="pdfTable">
    <tr>
        @foreach($columns as $column)
            <th>{{ $column }}</th>
        @endforeach
    </tr>
    @foreach($data as $dataObject)
        <tr>
            @foreach($columns as $column)
                <td><strong>{{$dataObject->$column}}</strong></td>
            @endforeach

        </tr>

    @endforeach
</table>

</body>
</html>


